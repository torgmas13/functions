const getSum = (str1, str2) => {
  if(typeof str1 !== 'string' || typeof str2 !== 'string'){
    return false;
  }
  var regExp = /[a-zA-Z]/g;
  if(regExp.test(str1) || regExp.test(str2)){
    return false;
  }
  return (+str1 + +str2).toString();
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let post = 0;
  let comment = 0;

  for(const it of listOfPosts){
    if(it.author == authorName){
      post += 1;
    }
    if(it.hasOwnProperty('comments')){
        for(const it2 of it.comments){
          if(it2.author == authorName){
            comment += 1
          }
        }
    }
  }
  return 'Post:' + post + ',comments:' + comment;
};

const tickets=(people)=> {
  if(people[0] !== 25){
    return 'NO';
  }

  let temp = people[0];

  for(let i = 1; i < people.length; i += 1){
    if(people[i] === 25){
      temp += people[i]
    }
    else{
      if(temp === 50 && people[i] > 50){
        return "NO";
      }
      temp += people[i];
      temp -= people[i] - 25;
    }
    if(temp < 0 || temp%25 !== 0){
      return 'NO';
    }
  }
  return "YES";
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
